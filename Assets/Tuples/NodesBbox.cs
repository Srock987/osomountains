﻿using Models;

namespace Tuples
{
        public struct NodesBbox
        {
                private OsmNode[] nodes;
                private BBox bBox;

                public NodesBbox(OsmNode[] nodes, BBox bBox)
                {
                        this.nodes = nodes;
                        this.bBox = bBox;
                }

                public OsmNode[] Nodes
                {
                        get { return nodes; }
                }

                public BBox BBox
                {
                        get { return bBox; }
                }
        }
}