﻿using Models;

namespace Tuples
{
        public struct HeightsBbox
        {
                private readonly float[,] heights;
                private readonly Sticker[] stickers;
                private readonly BBox bbox;

                public HeightsBbox(float[,] heights,Sticker[] stickers, BBox bbox)
                {
                        this.heights = heights;
                        this.stickers = stickers;
                        this.bbox = bbox;
                }

                public float[,] Heights
                {
                        get { return heights; }
                }

                public BBox Bbox
                {
                        get { return bbox; }
                }

                public Sticker[] Stickers
                {
                        get { return stickers;  }
                }

        }
}