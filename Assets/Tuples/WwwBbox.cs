﻿using Models;
using UnityEngine;

namespace Tuples
{
        public struct WwwBbox
        {
                private WWW www;
                private BBox bBox;

                public WwwBbox(WWW www, BBox bBox)
                {
                        this.www = www;
                        this.bBox = bBox;
                }

                public WWW Www
                {
                        get { return www; }
                }

                public BBox BBox
                {
                        get { return bBox; }
                }

                public WwwBbox CombineWith(WwwBbox otherBox)
                {
                        if (otherBox.BBox != null && www != null)
                        {
                                return new WwwBbox(www,otherBox.bBox);
                        }

                        if (otherBox.Www != null && bBox != null)
                        {
                                return new WwwBbox(otherBox.Www,bBox);
                        }
                        Debug.Log("NOT MERGED");
                        return new WwwBbox();
                }
        }
}