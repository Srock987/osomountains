﻿using System;
using System.Globalization;
using System.Linq;
using Models;
using Networking;
using Stickers;
using TerrainGeneration;
using Tuples;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Utils;


public class ActionCoordinator : MonoBehaviour{

    public GameObject statusPanel;
    public Text panelText;
    public Text gsptext;
    public string startText = "Waiting for orders";
    public TerrainModificator TerrainMode;
    public AccelerometerNavigation navigation;
    public StickerGenerator stickerGenerator;

    public void StartDataLoading()
    {
        panelText.text = "Querying location...";
        var observableBbox = BoxProvider.GetBox();
        observableBbox.Select(bbox =>
            {
            panelText.text = "Succesfully obtained user's location";
                var wwwObs = OverpassApiRequester.GetWwwNodes(bbox).Select(www => new WwwBbox(www,null));
                var boxObs = bbox.ToObservable().Select(forwardingBbox => new WwwBbox(null, forwardingBbox));
                return Observable.Zip(wwwObs, boxObs)
                    .Select(((list, i) => list[0].CombineWith(list[1])))
                    .SkipWhile(combinedWwwBox => combinedWwwBox.BBox == null || combinedWwwBox.Www == null);
            }).SelectMany(wwwbbox => wwwbbox)
          .Select(wwwbbox => {
                panelText.text = "Got web results";
            return new NodesBbox(JsonParser.ParseOsmJson(wwwbbox.Www),wwwbbox.BBox);})
            .Select(nodesBbox => new HeightsBbox(TerrainMode.GenerateHeights(nodesBbox),stickerGenerator.paseNodesToStickers(nodesBbox.Nodes, nodesBbox.BBox),nodesBbox.BBox))
          .SubscribeOnMainThread().Subscribe(heightsBbox =>
            {
            panelText.text = "Got web results";
            print("Number of stickers "+heightsBbox.Stickers.Length);
            print("Number of heights "+heightsBbox.Heights.Length);
             navigation.SetCameraAltitude((int) (heightsBbox.Bbox.Altitude *
                                                GeneratorProperties.MAP_TO_WOLD_RATIO));
             TerrainMode.UpdateData(heightsBbox.Heights);
             heightsBbox.Stickers.ToList().ForEach(sticker => stickerGenerator.placeSticker(sticker));
             panelText.text = heightsBbox.Bbox.Altitude.ToString(CultureInfo.InvariantCulture);
             statusPanel.SetActive(false);
            });
        
    }


    void Start()
    {
        //Text sets your text to say this message
        navigation.decText = gsptext;
        panelText.text = startText;
        StartDataLoading();
    }

}


