﻿using Models;
using UnityEngine;

namespace Networking
{
    public class JsonParser : MonoBehaviour {

        public static OsmNode[] ParseOsmJson(WWW www){
            OsmAnswer answer = OsmAnswer.CreateFromJson(www.text);
            return answer.elements;
        }
    }
}
