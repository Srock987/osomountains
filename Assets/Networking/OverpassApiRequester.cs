﻿using Models;
using UniRx;
using UnityEngine;

namespace Networking
{
    public class OverpassApiRequester : MonoBehaviour {

        private const string prefixUrl = "https://www.overpass-api.de/api/interpreter?data=[out:json][timeout:25];";
        private const string nodePart = "node[ele][natural=peak]";
        private const string relationPart = "relation[ele]";
        private const string suffix = "out%20meta;";
   

        public static IObservable<OsmNode[]> GetOsmNodes(BBox bBox){
            return GetWwwNodes(bBox).Select(JsonParser.ParseOsmJson);
        }

        public static IObservable<WWW> GetWwwNodes(BBox bBox){
            return ObservableWWW.GetWWW(BuildUrl(bBox));
        }

        public static IObservable<string> GetBuildUrl(BBox bbox){
            return Observable.Return<string>(BuildUrl(bbox));
        }



        public static string BuildUrl(BBox bBox){
            var bboxFormatted = BboxFormat(bBox);
            var url = prefixUrl;
            url += nodePart;
            url += bboxFormatted;
//            url += relationPart;
//            url += bboxFormatted;
//            url += nodePart;
//            url += bboxFormatted;
            url += suffix;
            return url;
        }

        private static string BboxFormat(BBox bBox){
            string bboxString = "";
            bboxString += "(";
            bboxString += bBox.BottomBound().ToString();
            bboxString += ",";
            bboxString += bBox.LeftBound().ToString();
            bboxString += ",";
            bboxString += bBox.TopBound().ToString();
            bboxString += ",";
            bboxString += bBox.RightBound().ToString();
            bboxString += ");";
            return bboxString;
        }



    }
}
