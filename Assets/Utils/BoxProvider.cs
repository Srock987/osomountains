﻿using System.Collections;
using Models;
using TerrainGeneration;
using UniRx;
using UnityEngine;

namespace Utils
{
    public class BoxProvider : MonoBehaviour {

        public static IObservable<BBox> GetBox(){
            return Observable.FromCoroutine<BBox>( GetBoxCore);
        }

        private static IEnumerator GetBoxCore(IObserver<BBox> observer, CancellationToken cancellationToken){
            if (Application.isEditor)
            {
                observer.OnNext(EditorBBox());
                observer.OnCompleted();
                yield break;
            }
            if (!Input.location.isEnabledByUser){
                observer.OnError(new System.ArgumentException("Location not enabled"));
                yield break;
            }
            Input.location.Start();
            int maxWaitTime = 20;
            while (Input.location.status == LocationServiceStatus.Initializing && maxWaitTime > 0 && !cancellationToken.IsCancellationRequested) {
                yield return new WaitForSeconds(1);
                maxWaitTime--;
            }
            if (cancellationToken.IsCancellationRequested) 
                yield break;
            if (maxWaitTime < 1){
                observer.OnError(new System.ArgumentException("Man wait time exceeded"));
                yield break;
            }
            if (LocationServiceStatus.Failed == Input.location.status)
                yield break;
            observer.OnNext(new BBox(Input.location.lastData.latitude, Input.location.lastData.longitude, Input.location.lastData.altitude, GeneratorProperties.SEARCH_RANGE));
            observer.OnCompleted();
        }

        private static BBox EditorBBox()
        {
            return new BBox(50.064651f,19.944981f,100f,GeneratorProperties.SEARCH_RANGE);
        }
    }
}
