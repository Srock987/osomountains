﻿using System;
using System.Collections;
using System.Globalization;
using System.Xml.Schema;
using TerrainGeneration;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

namespace Utils
{
	public class AccelerometerNavigation : MonoBehaviour
	{
		
		public Camera mainCamera;
		public GameObject panel;
		private Gyroscope gyro;
		private Compass compass;
		public Text decText;
		public GameObject compassImage;
		public Transform terrainTransform;
		
		private bool gyroEnabled;
		private bool compassEnabled;
		private bool movingForward;
		private bool isCalibrating;
		private float calibrationToFix;
		
		// SETTINGS
		private float smoothing = 0.1f;
	//	private float speed = 5f;
		
		// STATE
		private float initialYAngle = 0f;
		private float appliedGyroYAngle = 0f;
		private float calibrationYAngle = 0f;
		private Transform rawGyroRotation;
		private float tempSmoothing;
		private const float distanceCompasForward = 200f;
		private const float distanceCompasRight = 120f;
		private const float distanceCompasDown = 70f;

		private bool cameraAltitudeSet = true;
		private Vector3 cameraAltitudeDestigned;
		private Vector3 compassDir;

		private float headingChangeCountdown = 0f;
		private float headingChangeIntervals = 0.25f;
		

		// Use this for initialization
		private IEnumerator Start () {
			gyroEnabled = EnableGyro();
			compassEnabled = EnableCompass();
			//mainCamera.farClipPlane = GeneratorProperties.TERRAIN_SIZE;
			
			rawGyroRotation = new GameObject("GyroRaw").transform;
			rawGyroRotation.position = transform.position;
			rawGyroRotation.rotation = transform.rotation;
			
			yield return new WaitForSeconds(1);

			StartCoroutine(CalibrateYAngle());
			InitialCompassDirection();
		}

		public void Calibrate()
		{
			if (compassEnabled)
			{
				calibrationToFix = Input.compass.trueHeading;
			}
			isCalibrating = true;
		}

		public void InitialCompassDirection()
		{
			if (compassEnabled)
			{
				var currentHeading = Input.compass.trueHeading;
				Vector3 terrainDir = Vector3.zero;
				terrainDir.z = currentHeading;
				terrainTransform.localEulerAngles =
					Vector3.Slerp(terrainTransform.localEulerAngles, terrainDir, Time.deltaTime * 2f);
			}
		}

		// Update is called once per frame
		private void Update () {
				if (gyroEnabled)
				{
					ApplyGyroRotation();
					ApplyCalibration();
					transform.rotation = Quaternion.Slerp(transform.rotation, rawGyroRotation.rotation, smoothing);
//					compassGameObject.transform.position =
//						mainCamera.transform.position + mainCamera.transform.forward * distanceCompasForward  + mainCamera.transform.right * distanceCompasRight + mainCamera.transform.up * -distanceCompasDown;
				}
	
				if (compassEnabled)
				{
					
					//compassGameObject.transform.rotation = Quaternion.Slerp(compassGameObject.transform.rotation, new Quaternion(0f,yRotation , 0f, 1f) , smoothing);
					var currentHeading = Input.compass.trueHeading;
					headingChangeCountdown -= Time.deltaTime;
					if(headingChangeCountdown<0f){
						decText.text = currentHeading.ToString(CultureInfo.InvariantCulture);
						headingChangeCountdown = headingChangeIntervals;
					}
					
					compassDir.z = currentHeading;
					compassImage.transform.localEulerAngles = Vector3.Slerp(compassImage.transform.localEulerAngles,compassDir,Time.deltaTime*2f);
				}
				
				if (movingForward)
				{
					mainCamera.transform.Translate(mainCamera.transform.forward);
				}
	
				if (cameraAltitudeSet) return;
				mainCamera.transform.position = Vector3.MoveTowards(mainCamera.transform.position,cameraAltitudeDestigned,20f*Time.deltaTime);
				if (Math.Abs(mainCamera.transform.position.y - cameraAltitudeDestigned.y) < 0.1f)
				{
					cameraAltitudeSet = true;
				}
		}


		private void ApplyGyroRotation()
		{
			rawGyroRotation.rotation = Input.gyro.attitude;
			rawGyroRotation.Rotate(0f,0f,180f, Space.Self);
			rawGyroRotation.Rotate(90f, 180f, 0f, Space.World);
			appliedGyroYAngle = rawGyroRotation.eulerAngles.y;
		}
		
		private void ApplyCalibration()
		{
			rawGyroRotation.Rotate(0f,-calibrationYAngle, 0f, Space.World);
		}

		private IEnumerator CalibrateYAngle()
		{
			tempSmoothing = smoothing;
			smoothing = 1;
			calibrationYAngle = appliedGyroYAngle - initialYAngle;
			yield return null;
			smoothing = tempSmoothing;
		}

		private bool EnableGyro()
		{
			if (!SystemInfo.supportsGyroscope) return false;
			gyro = Input.gyro;
			gyro.enabled = true;
			return true;
		}
		
		private static bool EnableCompass()
		{
			if (Application.isEditor) return false;
			Input.compass.enabled = true;
			return Input.compass.enabled;

		}


		public void MoveForward()
		{
			panel.SetActive(false);
			movingForward = true;
		}

		public void StopMovement()
		{
			movingForward = false;
		}

		public void SetCameraAltitude(float altitude)
		{
			var realAltitude = terrainTransform.position.y + altitude;
			cameraAltitudeDestigned =
				new Vector3(mainCamera.transform.position.x, realAltitude, mainCamera.transform.position.z);
			cameraAltitudeSet = false;
		}
	}
}
