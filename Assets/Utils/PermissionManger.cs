﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_ANDROID || UNITY_EDITOR

namespace Utils
{
    public class PermissionManger : MonoBehaviour {

       // private static string[] permissionNames = { "android.permission.ACCESS_COARSE_LOCATION" };

        public Text statusText;

//        private static List<GvrPermissionsRequester.PermissionStatus> permissionList =
//            new List<GvrPermissionsRequester.PermissionStatus>();

        // Use this for initialization
        void Start () {
        
        }
	
        // Update is called once per frame
        void Update () {
		
        }

        public void CheckPermission()
        {
            statusText.text = "Checking permission....";
//            GvrPermissionsRequester permissionRequester = GvrPermissionsRequester.Instance;
//            if (permissionRequester != null)
//            {
//                bool granted = permissionRequester.IsPermissionGranted(permissionNames[0]);
//                statusText.text = permissionNames[0] + ": " + (granted ? "Granted" : "Denied");
//            }
//            else
//            {
//                statusText.text = "Permission requester cannot be initialized.";
//            }
        }

        public void RequestPermissions()
        {
            if (statusText != null)
            {
                statusText.text = "Requesting permission....";
            }
//            GvrPermissionsRequester permissionRequester = GvrPermissionsRequester.Instance;
//            if (permissionRequester == null)
//            {
//                statusText.text = "Permission requester cannot be initialized.";
//                return;
//            }
//            Debug.Log("Permissions.RequestPermisions: Check if permission has been granted");
//            if (!permissionRequester.IsPermissionGranted(permissionNames[0]))
//            {
//                Debug.Log("Permissions.RequestPermisions: Permission has not been previously granted");
//                if (permissionRequester.ShouldShowRational(permissionNames[0]))
//                {
//                    statusText.text = "This game needs to access location.  Please grant permission when prompted.";
//                    statusText.color = Color.red;
//                }
//                permissionRequester.RequestPermissions(permissionNames,
//                    (GvrPermissionsRequester.PermissionStatus[] permissionResults) =>
//                    {
//                        statusText.color = Color.cyan;
//                        permissionList.Clear();
//                        permissionList.AddRange(permissionResults);
//                        string msg = "";
//                        foreach (GvrPermissionsRequester.PermissionStatus p in permissionList)
//                        {
//                            msg += p.Name + ": " + (p.Granted ? "Granted" : "Denied") + "\n";
//                        }
//                        statusText.text = msg;
//                    });
//            }
            else
            {
                statusText.text = "ExternalStorage permission already granted!";
            }
        }

    }
}
#endif  // (UNITY_ANDROID || UNITY_EDITOR)