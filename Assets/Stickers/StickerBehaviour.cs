﻿using System.Globalization;
using UnityEngine;

namespace Stickers
{
	public class StickerBehaviour : MonoBehaviour
	{

		public TextMesh StickerName;
		public TextMesh StickerAltitude;
		private Camera mainCamera;

		// Use this for initialization
		void Start () {
		
		}
	
		// Update is called once per frame
		void Update () {
			transform.LookAt(Camera.main.transform.forward, Vector3.up);
		}

		public void SetName(string stickerName)
		{
			StickerName.text = stickerName;
		}

		public void SetAltitude(float altitude)
		{
			StickerAltitude.text = altitude.ToString(CultureInfo.InvariantCulture);
		}
	}
}
