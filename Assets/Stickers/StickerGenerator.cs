﻿using System;
using System.Globalization;
using System.Linq;
using Models;
using TerrainGeneration;
using UnityEngine;

namespace Stickers
{
    public class StickerGenerator : MonoBehaviour
    {
        public GameObject prefabSticker;
        public BBox BBox;

        private void Start()
        {
            
        }

        public Sticker[] paseNodesToStickers(OsmNode[] nodes, BBox bBox)
        {
            BBox = bBox;
            return nodes.Select(createSticker).ToArray();
        }
        
        private Sticker createSticker(OsmNode osmNode)
        {
            var y = (int) (double.Parse(osmNode.tags[OsmModeProperties.ELEVATION_KEY])*GeneratorProperties.INCLINE_RATIO);
            var xResize = (osmNode.lon-BBox.LeftBound())/(BBox.RightBound()-BBox.LeftBound());
            var zResize = (osmNode.lat-BBox.BottomBound())/(BBox.TopBound()-BBox.BottomBound());
            var x = Convert.ToInt32(GeneratorProperties.TERRAIN_SIZE*(-0.5 + xResize));
            var z = Convert.ToInt32(GeneratorProperties.TERRAIN_SIZE*(-0.5 + zResize));
            
            var vector = new Vector3(x,y,z);
            var altitude = osmNode.tags.ContainsKey(OsmModeProperties.ELEVATION_KEY)
                ? float.Parse(osmNode.tags[OsmModeProperties.ELEVATION_KEY])
                : 0;
            var peakName = osmNode.tags.ContainsKey(OsmModeProperties.NAME_KEY)
                ? osmNode.tags[OsmModeProperties.NAME_KEY]
                : "";
            return new Sticker(vector,altitude,peakName);
        }

        public void placeSticker(Sticker sticker)
        {
            var lastVector = sticker.Vector;
            var stickerBehavious = prefabSticker.GetComponent<StickerBehaviour>();
            stickerBehavious.SetName(sticker.Name);
            stickerBehavious.SetAltitude(sticker.Altitude);
            Instantiate(prefabSticker, lastVector, Quaternion.identity);
        }
    }
}
