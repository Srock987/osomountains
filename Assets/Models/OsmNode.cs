﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Models
{
//    [Serializable]
    public class OsmNode
    {
        public string type;
        public long id;
        public float lat;
        public float lon;
        public DateTime timestamp;
        public int version;
        public int changeset;
        public string user;
        public int uid;
        public Dictionary<string, string> tags;
        public static OsmNode CreateFromJson(string json){
            return JsonUtility.FromJson<OsmNode>(json);
        }

        public override string ToString()
        {
            return "OsmNode{" +
                   "type='" + type + '\'' +
                   ", id=" + id.ToString() +
                   ", lat=" + lat.ToString() +
                   ", lon=" + lon.ToString() +
                   ", timestamp=" + timestamp.ToString() +
                   ", version=" + version.ToString() +
                   ", changeset=" + changeset.ToString() +
                   ", user=" + user.ToString() +
                   ", uid=" + uid.ToString() +
                   ", " + printTags() +
                   '}';
        }

        private string printTags()
        {
            if (tags == null) return "null";
            var tagString = "tags={";
            foreach ( KeyValuePair<string,string> entry in tags)
            {
                tagString = tagString + entry.Key + " = " + entry.Value + ",";
            }
            tagString = tagString + "}";
            return tagString;
        }

    }

    public class OsmModeProperties
    {
        public const string ELEVATION_KEY = "ele";
        public const string NAME_KEY = "name";
        public const string NATURAL_KEY = "peak";
    }
}