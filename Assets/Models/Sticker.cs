﻿using UnityEngine;
using UnityEngine.Assertions.Comparers;

namespace Models
{
    public class Sticker
    {
        private readonly Vector3 vector;
        private readonly float altitude;
        private readonly string name;

        public Sticker( Vector3 vector,float altitude,string name)
        {
            this.vector = vector;
            this.altitude = altitude;
            this.name = name;
        }

        public Vector3 Vector
        {
            get { return vector; }
        } 

        public float Altitude
        {
            get { return altitude; }
        }

        public string Name
        {
            get { return name; }
        }
    }
}