﻿using UniRx;

namespace Models
{
	public class BBox
	{
		private float latitude;
		private float longitude;
		private float range;
		public BBox (float lat, float lon, float alt, float range)
		{
			this.latitude = lat;
			this.longitude = lon;
			this.Altitude = alt;
			this.range = range;
		}

		public float TopBound()
		{
			return normalizeLatitiude(latitude + range);
		}

		public float BottomBound()
		{
			return normalizeLatitiude(latitude - range/2);
		}

		public float LeftBound()
		{
			return normalizeLongitude(longitude - range/2);
		}

		public float RightBound()
		{
			return normalizeLongitude(longitude + range/2);
		}

		private float normalizeLongitude(float longitude1){
			return longitude1 % 90.0f;
		}

		private float normalizeLatitiude(float latitude1){
			return latitude1 % 90.0f;
		}

		public IObservable<BBox> ToObservable()
		{
			return Observable.Return<BBox>(this);
		}

		public float Altitude { get; private set; }


		public override string ToString()
		{
			return "BBox{" +
			       "longitude='" + longitude +
			       ", latitude=" + latitude +
			       ", altitude=" + Altitude +
			       '}';
		}
	}
}