﻿using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace Models
{
//    [System.Serializable]
    public class OsmAnswer{
        public float version;
        public string generator;
        public Dictionary<string, string> osm3s;
        public OsmNode[] elements;
        public static OsmAnswer CreateFromJson(string json)
        {
           return JsonConvert.DeserializeObject<OsmAnswer>(json);
//            return JsonUtility.FromJson<OsmAnswer>(json);
        }
    }
}