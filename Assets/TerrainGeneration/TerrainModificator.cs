﻿using System.Collections.Generic;
using System.Linq;
using HeightModels;
using Tuples;
using UnityEngine;

namespace TerrainGeneration
{
    public class TerrainModificator : MonoBehaviour {

        public Terrain terrain;

        private void Start()
        {
            terrain.terrainData.size=new Vector3(GeneratorProperties.TERRAIN_SIZE,GeneratorProperties.TERRAIN_SIZE,GeneratorProperties.TERRAIN_SIZE);
            terrain.terrainData.heightmapResolution = GeneratorProperties.TERRAIN_SIZE;
            transform.position = new Vector3(-0.5f*GeneratorProperties.TERRAIN_SIZE,0,-0.5f*GeneratorProperties.TERRAIN_SIZE);
        }

        public float[,] GenerateHeights(NodesBbox nodesBbox)
        {
            var bBox = nodesBbox.BBox;
            var worldSize = terrain.terrainData.size;
            var heightMapWidth = terrain.terrainData.heightmapWidth;
            var heightMapHeight = terrain.terrainData.heightmapHeight;
            var heights = terrain.terrainData.GetHeights(0, 0, heightMapWidth, heightMapHeight);
            var heightPoints = new List<HeightPoint>();
            var peaks = nodesBbox.Nodes.Select(node => HeightPointGenerator.SinglePeakOsmNode(node, bBox, heightMapWidth, heightMapHeight))
                .Select(peak => new PeakHeightPoint(peak)).ToList();
            var collidingPeaks = new List<PeakHeightPoint>();
            var groupedCollidingPeaks = new List<List<HeightPoint>>();
        
            //Detecting collision beetween peaks
            for (var i = 0; i < peaks.Count; i++)
            {
                for (var j = 0; j < peaks.Count; j++)
                {
                    if (i == j) continue;
                    if (PeakCollisionDetector.DetectCollision(peaks[i].GetPeak(), peaks[j].GetPeak()))
                    {
                        peaks[i].AddCollision(peaks[j].GetPeak());
                    }
                }
            }        

            for (var i = peaks.Count - 1; i >= 0; i--)
            {
                if (!peaks[i].GetCollisions().Any()) continue;
                collidingPeaks.Add(peaks[i]);
                peaks.RemoveAt(i);
            }

            foreach (var peakHeightPoint in peaks)
            {
                heightPoints.AddRange(HeightPointGenerator.CreateBasicMountain(peakHeightPoint.GetPeak()));
            }

            foreach (var collidingPeak in collidingPeaks)
            {
                if (groupedCollidingPeaks.Exists(group => @group.Contains(collidingPeak.GetPeak()))) continue;
                var collisionGroup = new List<HeightPoint>();
                collisionGroup = FindGroupedCollisions(collidingPeak.GetPeak(), collidingPeaks, collisionGroup,0);
                if (collisionGroup.Any())
                {
                    groupedCollidingPeaks.Add(collisionGroup); 
                }
            }

            if (groupedCollidingPeaks.Any())
            {
                heightPoints.AddRange(HeightPointGenerator.CreateCollidingMountain(groupedCollidingPeaks)); 
            }

            foreach (var point in heightPoints)
            {
                if (point.width < heightMapWidth && point.height < heightMapHeight && point.width >= 0 && point.height >= 0)
                {
                    heights[point.height, point.width] =   (float)point.altitude/GeneratorProperties.TERRAIN_SIZE; 
                }
            }
            
            return heights;
        }

        private static List<HeightPoint> FindGroupedCollisions(HeightPoint heightPoint, List<PeakHeightPoint> collidingPeaks, List<HeightPoint> currentGroup,int round)
        {
            if (currentGroup.Contains(heightPoint)) return currentGroup;
            currentGroup.Add(heightPoint);
            foreach (var point in collidingPeaks.Find( peakHeightPoint => peakHeightPoint.GetPeak().Equals(heightPoint) ).GetCollisions()
                .Where( collisions => !@currentGroup.Contains(collisions)))
            {
                var deepSearch = FindGroupedCollisions(point, collidingPeaks, currentGroup, round + 1);
                currentGroup.AddRange(deepSearch.Where( deepPoints => !currentGroup.Contains(deepPoints) ));
            }
            return currentGroup;
        }
    
    

        public void UpdateData(float[,] heights)
        {
            if (terrain.terrainData == null) return;
            terrain.terrainData.SetHeights(0,0,heights);
        }
    

    }
}
