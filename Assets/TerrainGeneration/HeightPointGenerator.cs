﻿using System;
using System.Collections.Generic;
using System.Linq;
using HeightModels;
using Models;
using UnityEngine;

namespace TerrainGeneration
{
        public static class HeightPointGenerator
        {
                public static HeightPoint SinglePeakOsmNode(OsmNode osmNode, BBox bBox,int heightMapWidth, int heightMapHeight)
                {
                        var altitude = (int) (double.Parse(osmNode.tags[OsmModeProperties.ELEVATION_KEY]));
                        var widthResize = (osmNode.lon-bBox.LeftBound())/(bBox.RightBound()-bBox.LeftBound());
                        var heightResize = (osmNode.lat-bBox.BottomBound())/(bBox.TopBound()-bBox.BottomBound());
                        var width = Convert.ToInt32(heightMapWidth*widthResize);
                        var height = Convert.ToInt32(heightMapHeight*heightResize);
                        return new HeightPoint(altitude,width,height);
                }

                public static IEnumerable<HeightPoint> CreateBasicMountain(HeightPoint peak)
                {
                        var basicMountain = new List<HeightPoint>();
                        var baseRadious = (int) (peak.altitude * GeneratorProperties.INCLINE_RATIO);
                        for (var i = 0; i < peak.altitude; i=i+GeneratorProperties.INCLINE_RENDERIND_FREQUENCY)
                        {
                                var radious = baseRadious - i;
                                var circlePoints = CirclePoints(peak.width, peak.height, radious);
                                basicMountain.AddRange(circlePoints.Select(circlePoint => new HeightPoint(i, circlePoint.X, circlePoint.Y)));
                        }
                        return basicMountain;
                }

                public static IEnumerable<HeightPoint> CreateCollidingMountain(List<List<HeightPoint>> groupedCollidingPeaks)
                {
                        var collidingMountain = new List<HeightPoint>();
                        foreach (var peaksGroup in groupedCollidingPeaks)
                        {
                                var maximumAltitude = peaksGroup.Max(heightPoint => heightPoint.altitude);
                                for (var i = 0; i < maximumAltitude; i=i+GeneratorProperties.INCLINE_RENDERIND_FREQUENCY)
                                {
                                        var mountainPoints = new List<Point>();
                                        foreach (var peak in peaksGroup)
                                        {
                                                var baseRadious = (int) (peak.altitude * GeneratorProperties.INCLINE_RATIO);
                                                var radious = baseRadious - i;
                                                mountainPoints = MergedCirclePoints(mountainPoints,peak,radious).ToList();
                                        }
                                        collidingMountain.AddRange(mountainPoints.Select(mountainPoint => new HeightPoint(i,mountainPoint.X,mountainPoint.Y)));
                                }
                        }
                        return collidingMountain;
                }

                private static IEnumerable<Point> CirclePoints(int xMiddle, int yMiddle , int radious)
                {
                        var levelPoints = new List<Point>();
                        for (var x = -radious; x < radious ; x++)
                        {
                                for (var y = -radious; y < radious; y++)
                                {
                                        if (Math.Sqrt(Math.Pow(Convert.ToDouble(x),2.0) + Math.Pow(Convert.ToDouble(y),2)) <= Convert.ToDouble(radious))
                                        {
                                                levelPoints.Add(new Point(xMiddle+x,yMiddle+y));
                                        }
                                }
                        }
                        return levelPoints;
                }

                private static IEnumerable<Point> MergedCirclePoints(List<Point> currentMountain, HeightPoint unmergedPeak, int altitude)
                {
                        currentMountain.AddRange(CirclePoints(unmergedPeak.width,unmergedPeak.height,altitude));
                        return currentMountain;
                }
        }
}