﻿namespace TerrainGeneration
{
        public static class GeneratorProperties
        {
                public const int TERRAIN_SIZE = 2048;
                public const double INCLINE_RATIO = 0.2;
                public const int INCLINE_RENDERIND_FREQUENCY = 1;
                public const double HEIGHT_TRANSFER_RATIO = 0.4/INCLINE_RATIO;
                public const float MAP_TO_WOLD_RATIO = 0.2f;
                public const float SEARCH_RANGE = 0.2f;

        }
}
