﻿using System;
using HeightModels;

namespace TerrainGeneration
{
    public static class PeakCollisionDetector
    {
        public static bool DetectCollision(HeightPoint peak1, HeightPoint peak2)
        {
            var peakDistance = Math.Sqrt(Math.Pow(peak1.height - peak2.height, 2.0) + Math.Pow(peak1.width - peak2.width,2.0));
//                Debug.Log("Distance between " + peakDistance);
            var summedRadious = Convert.ToDouble(peak1.altitude + peak2.altitude)*GeneratorProperties.INCLINE_RATIO;
            //Debug.Log("Summed radious " + summedRadious);
            var isColliding = (peakDistance < summedRadious);
            //Debug.Log("Is colliding " + isColliding);
            return isColliding;
        }
    }
}