﻿using System;

namespace HeightModels
{
     public struct HeightPoint
     {
          public HeightPoint(int alt, int wid,int hei)
          {
               this.altitude = alt;
               this.width = wid;
               this.height = hei;
          }
          public readonly int altitude;
          public readonly int width;
          public readonly int height;

          public override string ToString()
          {
               return "{altitude=" + altitude +
                      ", width=" + width +
                      ", height=" + height + "}";
          }

          public override bool Equals(object obj)
          {
               if (obj == null) return false;
               var item = (HeightPoint) obj;
               return this.altitude == item.altitude && this.width == item.width && this.height == item.height;
          }

          public override int GetHashCode()
          {
               return base.GetHashCode();
          }
     }
}