﻿namespace HeightModels
{
        public class Point
        {
                public readonly int X;
                public readonly int Y;

                public Point(int x, int y)
                {
                        this.X = x;
                        this.Y = y;
                }

                protected bool Equals(Point other)
                {
                        return X == other.X && Y == other.Y;
                }

                public override int GetHashCode()
                {
                        unchecked
                        {
                                return (X * 397) ^ Y;
                        }
                }
        }
}