﻿namespace HeightModels
{
        public struct Collision
        {
                public HeightPoint collisionPoint;
                public bool isMerged;
                public Collision(HeightPoint collisionPoint)
                {
                        this.collisionPoint = collisionPoint;
                        this.isMerged = false;
                }
        }
}