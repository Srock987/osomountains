﻿using System.Collections.Generic;

namespace HeightModels
{
    public struct PeakHeightPoint
    {
        private readonly HeightPoint heightPoint;
        private readonly List<Collision> collisions;
        public PeakHeightPoint(HeightPoint heightPoint)
        {
            this.heightPoint = heightPoint;
            this.collisions = new List<Collision>();
        }

        public HeightPoint GetPeak()
        {
            return this.heightPoint;
        }

        public void AddCollision(HeightPoint collidingPoint)
        {
            collisions.Add(new Collision(collidingPoint));
        }

        public List<HeightPoint> GetCollisions()
        {
            return collisions.ConvertAll( collision => collision.collisionPoint);
        }
    }
}